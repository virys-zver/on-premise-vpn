#!/bin/bash -e

echo
echo "=== Install vpn server IPsec IKEv2 ==="
echo

function exit_badly {
  echo "$1"
  exit 1
}

OSVERSION=$(lsb_release -rs)
[[ "${OSVERSION}" == "18.04" ]] \
  || [[ "${OSVERSION}" == "20.04" ]] \
  || [[ "${OSVERSION}" == "22.04" ]] \
  || [[ "${OSVERSION}" == "10" ]] \
  || [[ "${OSVERSION}" == "11" ]] \
  || [[ "${OSVERSION}" == "12" ]] \
  || exit_badly "This script is for Ubuntu 18.04/20.04/22.04 and Debian 10/11/12 only: aborting"

[[ $(id -u) -eq 0 ]] || exit_badly "Please run this script as root (e.g. sudo ./path/to/this/script)"

export DEBIAN_FRONTEND=noninteractive

apt-get -o Acquire::ForceIPv4=true update
apt-get -o Acquire::ForceIPv4=true install -y software-properties-common

if [[ $(lsb_release -si) == "Ubuntu" ]]; then
  add-apt-repository -y universe
  add-apt-repository -y restricted
  add-apt-repository -y multiverse
fi

apt-get -o Acquire::ForceIPv4=true install -y moreutils dnsutils

echo
echo "--- Configuration: VPN settings ---"
echo

ETH0ORSIMILAR=$(ip route get 1.1.1.1 | grep -oP ' dev \K\S+')
IP=$(dig -4 +short myip.opendns.com @resolver1.opendns.com)

echo "Network interface: ${ETH0ORSIMILAR}"
echo "External IP: ${IP}"

read -r -p "IP address of the head server VPN: " VPNIP
read -r -p "Pool of VPN addresses of the head server (default: 10.100.0.0/16): " VPNIPPOOLMAIN
read -r -p "Pool of VPN addresses for a client (e.g. 10.101.0.0/16): " VPNIPPOOL

VPNIPPOOLMAIN=${VPNIPPOOLMAIN:-10.100.0.0/16}

while true; do
  read -r -s -p "PSK secret for VPN: " VPNPSK
  echo
  read -r -s -p "Confirm PSK secret for VPN: " VPNPSK2
  echo
  [[ "${VPNPSK}" = "${VPNPSK2}" ]] && break
  echo "Passwords didn't match -- please try again"
done

apt-get -o Acquire::ForceIPv4=true --with-new-pkgs upgrade -y
apt autoremove -y

apt-get -o Acquire::ForceIPv4=true install -y \
  iptables-persistent \
  strongswan libstrongswan-standard-plugins strongswan-libcharon libcharon-extra-plugins

# in 22.04 libcharon-standard-plugins is replaced with libcharon-extauth-plugins
apt-get -o Acquire::ForceIPv4=true install -y libcharon-standard-plugins \
  || apt-get -o Acquire::ForceIPv4=true install -y libcharon-extauth-plugins


echo
echo "--- Configuring firewall ---"
echo

# firewall

iptables -P INPUT   ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT  ACCEPT

iptables -F
iptables -t nat -F
iptables -t mangle -F

# INPUT

# accept anything already accepted
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# accept anything on the loopback interface
iptables -A INPUT -i lo -j ACCEPT

# drop invalid packets
iptables -A INPUT -m state --state INVALID -j DROP

# rate-limit repeated new requests from same IP to any ports
iptables -I INPUT -i "${ETH0ORSIMILAR}" -m state --state NEW -m recent --set
iptables -I INPUT -i "${ETH0ORSIMILAR}" -m state --state NEW -m recent --update --seconds 300 --hitcount 60 -j DROP

# accept (non-standard) SSH
iptables -A INPUT -p tcp --dport 22 -j ACCEPT


# VPN

# accept IPSec/NAT-T for VPN (ESP not needed with forceencaps, as ESP goes inside UDP)
iptables -A INPUT -p udp --dport  500 -j ACCEPT
iptables -A INPUT -p udp --dport 4500 -j ACCEPT
iptables -A INPUT -s "${VPNIPPOOL}" -j ACCEPT
iptables -A INPUT -s "${VPNIPPOOLMAIN}" -j ACCEPT

# reduce MTU/MSS values for dumb VPN clients
iptables -t mangle -A FORWARD --match policy --pol ipsec --dir in -s "${VPNIPPOOL}" -o "${ETH0ORSIMILAR}" -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360


# fall through to drop any other input and forward traffic

iptables -A INPUT   -j DROP
iptables -A FORWARD -j DROP

iptables -L

netfilter-persistent save

echo
echo "--- Configuring VPN ---"
echo

# ip_forward is for VPN
# ip_no_pmtu_disc is for UDP fragmentation
# others are for security

grep -Fq 'IKEv2-setup' /etc/sysctl.conf || echo "
# IKEv2-setup
net.ipv4.ip_forward = 1
net.ipv4.ip_no_pmtu_disc = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
net.ipv6.conf.${ETH0ORSIMILAR}.disable_ipv6 = 1
" >> /etc/sysctl.conf

sysctl -p

echo "config setup
  strictcrlpolicy=yes
  uniqueids=never

conn roadwarrior
  auto=start
  compress=no
  type=tunnel
  keyexchange=ikev2
  fragmentation=yes
  forceencaps=yes
  ike=aes256-sha1-modp1024!
  esp=aes256-sha1!
  authby=psk
  dpdaction=clear
  dpddelay=900s
  rekey=no
  left=%any
  leftid=${IP}
  leftsubnet=${VPNIPPOOL},${VPNIPPOOLMAIN}
  right=%any
  rightid=%any
  rightsourceip=${VPNIPPOOL}

conn host-host
  keyexchange=ikev2
  left=${IP}
  leftsubnet=${VPNIPPOOL}
  authby=psk
  leftfirewall=yes
  right=${VPNIP}
  rightsubnet=${VPNIPPOOLMAIN}
  type=tunnel
  ike=aes256-sha1-modp1024!
  esp=aes256-sha1!
  auto=start
" > /etc/ipsec.conf

echo "${IP} %any : PSK \"${VPNPSK}\"
" > /etc/ipsec.secrets

ipsec restart

cat << EOF > vpn-client.sh
#!/bin/bash -e
if [[ \$(id -u) -ne 0 ]]; then echo "Please run as root (e.g. sudo ./path/to/this/script)"; exit 1; fi

apt-get install -y strongswan libstrongswan-standard-plugins libcharon-extra-plugins
apt-get install -y libcharon-standard-plugins || true  # 17.04+ only

IP=\$(dig -4 +short myip.opendns.com @resolver1.opendns.com)

grep -Fq 'IKEv2-setup' /etc/ipsec.conf || echo "
# IKEv2-setup
conn ikev2vpn
  authby=psk
  leftfirewall=yes
  ikelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev2
  ike=aes256-sha1-modp1024!
  esp=aes256-sha1!
  leftsourceip=%config
  right=${IP}
  rightid=${IP}
  rightsubnet=${VPNIPPOOL},${VPNIPPOOLMAIN}
  auto=start
" >> /etc/ipsec.conf

grep -Fq 'IKEv2-setup' /etc/ipsec.secrets || echo "
# IKEv2-setup
\${IP} ${IP} : PSK "${VPNPSK}"
" >> /etc/ipsec.secrets

echo "Bringing up VPN ..."
ipsec restart

sleep 3

ipsec statusall

EOF

echo
echo "== Ubuntu, Debian =="
echo

echo "A bash script to set up strongSwan as a VPN client is attached as vpn-client.sh. You will need to chmod +x and then run the script as root."


echo
echo "--- How to connect ---"
echo
echo "Connection instructions can be found in the script's startup directory, $PWD."

echo "Run the command on the client server to copy the vpn installation file: scp root@${IP}:$PWD/vpn-client.sh ./"
echo "Run the command on client: bash vpn-client.sh"