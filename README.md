# on-premise-vpn

Script for deploying a VPN server in the customer's network

`` Before starting, you need to obtain information on the following points ``
- [ ] IP address of the head VPN server
- [ ] Subnet of the head VPN server
- [ ] Subnet assigned in the customer’s circuit
- [ ] Psk key of the head VPN server

## Getting started

Supported operating systems: Ubuntu 18.04 and above, Debian 10 and above

### Run 
```bash
wget https://gitlab.com/virys-zver/on-premise-vpn/-/raw/main/scripts/install.sh -O install.sh
bash install.sh
```

***

After installing the vpn server, the vpn-client.sh file will appear in the installation launch directory, which will need to be downloaded to all clients of this vpn server necessary to connect to the server and run on them.